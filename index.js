const header = document.querySelector('header')
const menu = document.querySelector('#menu-icon')
const navlist = document.querySelector('.navlist')
const impressumtoggler = document.querySelector('.impressum-toggler')
const impressum = document.querySelector('#impressum')

window.addEventListener('scroll', () => {
    header.classList.toggle('sticky', window.scrollY > 100);
})

menu.onclick = () => {
    menu.classList.toggle('bx-x');
    navlist.classList.toggle('open');
};

impressumtoggler.onclick = () => {
    impressum.classList.toggle('open');
}

window.onscroll = () => {
    menu.classList.remove('bx-x');
    navlist.classList.remove('open');
}